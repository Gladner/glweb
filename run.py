

from flask import Flask, render_template

app = Flask(  # Create a flask app
    __name__,
    template_folder='templates',  # Name of html file folder
    static_folder='static'  # Name of directory for static files
)

@app.route('/')  # Default site
def home_page():
    return render_template(
        'home.html',  # Template file path
    )
@app.route('/python')  # Default site
def python_page():
    return render_template(
        'python.html',  # Template file path
    )


@app.route('/store')
def store_page():
    return render_template('store.html')

@app.route('/store_bees')
def store_bees_page():
    return render_template('store_bees.html')


@app.route('/blogs')
def blogs_page():
    return render_template('blogs.html')


@app.route('/blogs_christian')
def blogs_christian_page():
    return render_template('blogs_christian.html')

app.run(  # Starts the site
    host='127.0.0.1',  # EStablishes the host
    port=5000  # Use port 5000
)